import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchMessage } from '../actions/auth';

class Feature extends Component {
  componentWillMount() {
    this.props.fetchMessage();
  }

  render() {
    return (
      <div>
        {this.props.user ? this.props.user.email : ''}
        {this.props.user ? this.props.user.role : ''}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  }
}

export default connect(mapStateToProps, { fetchMessage })(Feature);